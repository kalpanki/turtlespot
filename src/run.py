#!/usr/bin/env python

# for ROS
import rospy
import actionlib
from actionlib import GoalStatus
from geometry_msgs.msg import Pose, Point, Quaternion, Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionFeedback
from tf.transformations import quaternion_from_euler
from visualization_msgs.msg import Marker
from std_msgs.msg import String

# for state machine stuff
import smach
from smach import State, StateMachine
from smach_ros import SimpleActionState, IntrospectionServer

# for greeting 
import roslib; roslib.load_manifest('sound_play')
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

# generic stuff
import sys
import os
import subprocess
import signal
from math import  pi
from math import copysign
from collections import OrderedDict
import subprocess
import signal

# for AR tags
from ar_track_alvar.msg import AlvarMarkers

from face_recognition.msg import FaceRecognitionActionResult, FRClientGoal


class Run():
	def __init__(self):
		rospy.init_node('TurtleSpot', anonymous=False)
		rospy.on_shutdown(self.shutdown)
		# to stop robot at shutdown
		self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)

		# Some varaible to initialize and use elsewhere
		self.n_succeeded = 0
		self.n_aborted = 0

		rospy.loginfo("In Run - initializing waypoints to navigate")
		nav_states = list()
		self.waypoints = list()
		quaternions = list()
		
		# CHANGE WITH CARE...BEST NOT TO CHANGE !! 
		euler_angles = (pi/2, 3*pi/2, pi, 0)

	    # Then convert the angles to quaternions
		for angle in euler_angles:
			q_angle = quaternion_from_euler(0, 0, angle, axes='sxyz')
			q = Quaternion(*q_angle)
			quaternions.append(q)

		# all our waypoints 
		self.waypoints.append(Pose(Point(-0.612721562386, 4.1807050705, 0.0), quaternions[0])) # pi/2 
		self.waypoints.append(Pose(Point(0.618010520935, 1.96306490898, 0.0), quaternions[1])) # 0
		self.waypoints.append(Pose(Point(-0.810102939606, 0.453676611185, 0.0), quaternions[2])) # pi
		self.waypoints.append(Pose(Point(1.21015560627, 0.511729836464, 0.0), quaternions[3])) # 0
		# self.waypoints.append(Pose(Point(0.0, 2.0, 0.0), quaternions[3]))
		# self.waypoints.append(Pose(Point(2.0,2.0, 0.0), quaternions[0]))
		# self.waypoints.append(Pose(Point(0.0,2.0, 0.0), quaternions[1]))
		# self.waypoints.append(Pose(Point(0.0,0.0, 0.0), quaternions[2]))

		for waypoint in self.waypoints:
			nav_goal = MoveBaseGoal()
			nav_goal.target_pose.header.frame_id = 'map'
			nav_goal.target_pose.pose = waypoint
			rospy.loginfo("Adding waypoint " + str(waypoint))
			move_base_state = SimpleActionState('move_base', MoveBaseAction, 
												 goal=nav_goal, result_cb=self.move_base_result_cb,
		                                         exec_timeout=rospy.Duration(60.0),
		                                         server_wait_timeout=rospy.Duration(60.0))
			nav_states.append(move_base_state)

		rospy.loginfo("In Run - initializing markers for waypoints")
		self.init_waypoint_markers()
		for waypoint in self.waypoints:
			p = Point()
			p = waypoint.position
			self.waypoint_markers.points.append(p)

			self.marker_pub.publish(self.waypoint_markers)
			rospy.sleep(1)
			self.marker_pub.publish(self.waypoint_markers)

		# initialize the state machine states
		rospy.loginfo("In Run - initializing state machine states")
		self.sm_output = StateMachine(outcomes=['happy','sad'])
		# all variables for state machine communication
		#self.sm_output.userdata.target_detected = 0

		with self.sm_output:
			# state 1: Move to greeting state
			StateMachine.add('GREET', Greet(), transitions={'complete'   :'SEE_TARGET',
															'incomplete' : 'STOP'})

			# state 2: Note down the target to search
			StateMachine.add('SEE_TARGET', SeeTarget(), transitions={'ready' : 'RECOGNITION_CON',
																	'not_ready' : 'STOP'})

			# state 3: Concurrent container for running 
			# face recognition and voice control
			recog_con = smach.Concurrence(outcomes=['move','dont_move'],
										   default_outcome='dont_move',
										   outcome_map={'move' : { 'FaceRecognition' : 'found', 
										   						    'VoiceControl'   : 'move'}})
			with recog_con:
				smach.Concurrence.add('FaceRecognition', FaceRecognition())
				smach.Concurrence.add('VoiceControl', VoiceControl())
			# finally add the container
			smach.StateMachine.add('RECOGNITION_CON',recog_con,
							transitions={'dont_move' : 'STOP',
							 			'move' : 'WAYPOINT_1'})
			# End of state 3: RECOGNITION

			# state 4: Navigate to waypoint 1
			StateMachine.add('WAYPOINT_1', nav_states[0], 
				transitions={'succeeded':'SEARCH_WP1','aborted':'STOP','preempted':'STOP'})

			# State 5: Search at waypoint 1
			StateMachine.add('SEARCH_WP1', SearchWpOne(),
				transitions={'found' : 'FOLLOWER', 'not_found' : 'WAYPOINT_2'})

			# state 6: Navigate to waypoint 2
			StateMachine.add('WAYPOINT_2', nav_states[1], 
				transitions={'succeeded':'SEARCH_WP2','aborted':'STOP','preempted':'STOP'})

			# state 7: Search at waypoint 2
			StateMachine.add('SEARCH_WP2', SearchWpTwo(),
				transitions={'found' : 'FOLLOWER', 'not_found' : 'WAYPOINT_3'})

			# state 8: Navigate to waypoint 3
			StateMachine.add('WAYPOINT_3', nav_states[2], 
				transitions={'succeeded':'SEARCH_WP3','aborted':'STOP','preempted':'STOP'})

			# state 9: Search at waypoint 3
			StateMachine.add('SEARCH_WP3', SearchWpThree(),
				transitions={'found' : 'FOLLOWER', 'not_found' : 'WAYPOINT_4'})

			# state 10: Navigate to waypoint 4
			StateMachine.add('WAYPOINT_4', nav_states[3], 
				transitions={'succeeded':'SEARCH_WP4','aborted':'STOP','preempted':'STOP'})
			
			# state 11: Search at waypoint 3
			StateMachine.add('SEARCH_WP4', SearchWpFour(),
				transitions={'found' : 'FOLLOWER', 'not_found' : 'STOP'})

			# state 11: Follower
			StateMachine.add('FOLLOWER', FollowTag(), 
				transitions={'succeeded' : 'STOP', 'failed' : 'sad'})

			# Final state to STOP the robot in case of any disaster
			StateMachine.add('STOP', StopRobot(), 
				transitions={'succeeded' : 'happy'})

			# In order to visualize in smach_viewer
			rospy.loginfo("In Run - Starting the intro_server")
			intro_server = IntrospectionServer('patrol', self.sm_output, '/SM_ROOT')
			intro_server.start()

			# run the state machine and stop
			rospy.loginfo("In Run - STARTING THE STATE MACHINE")
			sm_outcome = self.sm_output.execute()
			rospy.loginfo("In Run - State Machine outcome is: " + str(sm_outcome))
			#rospy.spin()

			intro_server.stop()
			return

	def init_waypoint_markers(self):
		# Set up our waypoint markers
		marker_scale = 0.5
		marker_lifetime = 0 # 0 is forever
		marker_ns = 'waypoints'
		marker_id = 0
		marker_color = {'r': 1.0, 'g': 0.7, 'b': 1.0, 'a': 1.0}

		# Define a marker publisher.
		self.marker_pub = rospy.Publisher('waypoint_markers', Marker)

		# Initialize the marker points list.
		self.waypoint_markers = Marker()
		self.waypoint_markers.ns = marker_ns
		self.waypoint_markers.id = marker_id
		self.waypoint_markers.type = Marker.CUBE_LIST
		self.waypoint_markers.action = Marker.ADD
		self.waypoint_markers.lifetime = rospy.Duration(marker_lifetime)
		self.waypoint_markers.scale.x = marker_scale
		self.waypoint_markers.scale.y = marker_scale
		self.waypoint_markers.color.r = marker_color['r']
		self.waypoint_markers.color.g = marker_color['g']
		self.waypoint_markers.color.b = marker_color['b']
		self.waypoint_markers.color.a = marker_color['a']

		self.waypoint_markers.header.frame_id = 'map'
		self.waypoint_markers.header.stamp = rospy.Time.now()
		self.waypoint_markers.points = list()

	def move_base_result_cb(self, userdata, status, result):
		if status == actionlib.GoalStatus.SUCCEEDED:
			self.n_succeeded += 1
		elif status == actionlib.GoalStatus.ABORTED:
			self.n_aborted += 1
		elif status == actionlib.GoalStatus.PREEMPTED:
			self.n_preempted += 1

		try:
			rospy.loginfo("Success rate: " + 
				str(100.0 * self.n_succeeded / (self.n_succeeded + self.n_aborted  + self.n_preempted)))
		except:
			pass

	def shutdown(self):
			rospy.loginfo("Stopping the robot...")
			self.sm_output.request_preempt()
			self.cmd_vel_pub.publish(Twist())
			rospy.sleep(1)



class Greet(smach.State):
	def __init__(self):
		smach.State.__init__(self, outcomes=['complete','incomplete'])

	def execute(self, userdata):
		try:
			rospy.loginfo('In Greet - Executing state Greet')
			sc = SoundClient()
			rospy.sleep(2)
			sc.say('hello i am turtlespot i am at your service','voice_kal_diphone')
			rospy.sleep(10)
			sc.playWave('/home/turtlebot/ros/hydro/catkin_ws/src/rbx2/rbx2_tasks/nodes/dog_bark.wav')
			#sc.playWave('/home/shrini/catkin_ws/src/TurtleSpot/src/dog_bark.wav')
			
			return 'complete'
		except:
			rospy.loginfo("In Greet - Exception.")
			return 'incomplete'


class SeeTarget(smach.State):
	def __init__(self):
		smach.State.__init__(self, outcomes=['ready','not_ready'],
								output_keys=['target_marker_id'])
		rospy.Subscriber("ar_pose_marker", AlvarMarkers, self.target_pose_cb)
		# marker to detect
		self.marker_id = ''

	def execute(self, userdata):
		rospy.loginfo('In SeeTarget - Executing state Greet')
		sc = SoundClient()
		rospy.sleep(2)
		#sc.say('Please show me my target','voice_kal_diphone')
		try:
			# wait for 10 seconds to get the marker to search for
			for i in range(10):
				rospy.loginfo("In SeeTarget - for loop")
				sc.say('Please show me my target','voice_kal_diphone')
				rospy.sleep(2)
				if self.marker_id != '':
					userdata.target_marker_id = self.marker_id
					rospy.loginfo("In SeeTarget - ready to search. Marker id is: " + str(self.marker_id))
					sc.say('Target is set','voice_kal_diphone')
					rospy.sleep(2)
					sc.say('Ready to move','voice_kal_diphone')
					rospy.sleep(2)
					return 'ready'
				rospy.sleep(1.0)
			# we didn't see any marker, lets default
			rospy.loginfo("In SeeTarget - setting default tag to search: 101")
			userdata.target_marker_id = 101

			rospy.sleep(1.0)
			return 'ready'
		except:
			rospy.loginfo("In SeeTarget - Exception.")
			return 'not_ready'

	def target_pose_cb(self, msg):
		# we assume only one marker is shown to detect
		for i in range(len(msg.markers)):
			marker = msg.markers[i]
			if (marker != ''):
				self.marker_id = marker.id



class FaceRecognition(smach.State):
	def __init__(self):
		smach.State.__init__(self, outcomes = ['found', 'not_found'])
		self.face_cmd = FRClientGoal()
		# recognize only once
		self.face_cmd.order_id = 0
		self.face_cmd.order_argument = "none"
		self.face_detected = False
		self.face_found = 0

	def execute(self, userdata):
		rospy.loginfo("In FaceRecognition - executing state")

		self.procServ = subprocess.Popen('rosrun face_recognition Fserver _confidence_value:=0.7', shell=True, preexec_fn=os.setsid)
		self.procClient = subprocess.Popen('rosrun face_recognition Fclient', shell=True, preexec_fn=os.setsid)

		rospy.sleep(2)
		self.client_goal = rospy.Publisher('/fr_order', FRClientGoal,latch=True)
		self.client_goal.publish(self.face_cmd)
		self.face_fb =  rospy.Subscriber("/face_recognition/result", FaceRecognitionActionResult, self.face_recog_cb)

		for i in range(20):
			rospy.loginfo("In FaceRecognition - detecting face")
			if (self.face_found == 1):
				# kill process and return success
				rospy.sleep(5)
				self.face_fb.unregister()
				os.killpg(self.procClient.pid, signal.SIGTERM)
				os.killpg(self.procServ.pid, signal.SIGTERM)
				sc = SoundClient()
				rospy.sleep(5)
				sc.say('Your Face is recognised','voice_kal_diphone')
				rospy.sleep(2)
				sc.say('Hello Master! ','voice_kal_diphone')
				rospy.sleep(2)
				sc.playWave('/home/turtlebot/ros/hydro/catkin_ws/src/rbx2/rbx2_tasks/nodes/dog_bark.wav')
				rospy.loginfo("In FaceRecognition - returninig success")
				rospy.sleep(5)
				return 'found'
			rospy.sleep(3.0)

		# done with face recognition
		self.face_fb.unregister()
		os.killpg(self.procClient.pid, signal.SIGTERM)
		os.killpg(self.procServ.pid, signal.SIGTERM)

		return 'not_found'

	def face_recog_cb(self, msg):
		
		# if str(msg.result.names[0]) == "Shrini" or str(msg.result.names[0]) == "Richa":
		if msg.result.names[0] is not None:
			self.face_found = 1
			rospy.loginfo("Found person: " + str(msg.result.names[0]))


class VoiceControl(smach.State):
	def __init__(self):
		smach.State.__init__(self, outcomes = ['move', 'dont_move'])
		self.can_go = False

	def execute(self, userdata):
		rospy.loginfo("In VoiceControl - executing state")
		# launch the voice process
		voice = subprocess.Popen('roslaunch pocketsphinx robocup.launch', shell=True, preexec_fn=os.setsid)
		rospy.sleep(5)
		self.fb =  rospy.Subscriber("/recognizer/output", String, self.voice_cb)
		# check voice command for a few seconds
		for i in range(10):
			if self.can_go:
				self.fb.unregister()
				os.killpg(voice.pid, signal.SIGTERM)
				sc = SoundClient()
				sc.playWave('/home/turtlebot/ros/hydro/catkin_ws/src/rbx2/rbx2_tasks/nodes/dog_bark.wav')
				rospy.sleep(5)
				return 'move'
			rospy.sleep(2.0)

		# no luck with voice. kill the process and move the robot
		self.fb.unregister()
		os.killpg(voice.pid, signal.SIGTERM)
		rospy.loginfo("In VoiceControl - no luck with voice. Still moving on")
		return 'move'

	def voice_cb(self, msg):
		# due to poor performance, algorithm returns "no" if we say go :)
		if msg.data == "go" or msg.data == "no":
			self.can_go = True


class SearchWpOne(smach.State):
	def __init__(self):
		smach.State.__init__(self, outcomes = ['found', 'not_found'],
								 input_keys = ['target_marker_id'])
		self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)
		
		self.angular_vel = 1.0
		# if we are facing target, we will stop spinning
		self.facing_target = False
		self.target_id = ''

	def execute(self, userdata):
		rospy.loginfo('In SearchWpOne - executing state machine')
		rospy.Subscriber("ar_pose_marker", AlvarMarkers, self.target_pose_cb)
		rospy.sleep(3)
		rospy.loginfo("In SearchWpOne - target_marker_id is" + str(userdata.target_marker_id))
		self.target_id = userdata.target_marker_id
		
		for i in range(20):
			# if we found the target, stop and exit spinning
			rospy.loginfo("in for loop")
			if self.facing_target:
				sc = SoundClient()
				rospy.sleep(5)
				sc.playWave('/home/turtlebot/ros/hydro/catkin_ws/src/rbx2/rbx2_tasks/nodes/dog_bark.wav')
				#sc.playWave('/home/shrini/catkin_ws/src/TurtleSpot/src/dog_bark.wav')
				rospy.sleep(5)
				rospy.loginfo("Facing target. stopping spin and returning")
				return 'found'
			# else, keep searching by spinning
			move_cmd = Twist()
			move_cmd.linear.x = 0.0
			move_cmd.angular.z = self.angular_vel

			self.cmd_vel_pub.publish(move_cmd)
			rospy.sleep(0.75)

		# we finished spinning though we didn't find anything
		sc = SoundClient()
		sc.playWave('/home/turtlebot/ros/hydro/catkin_ws/src/rbx2/rbx2_tasks/nodes/dog_bark.wav')
		#sc.playWave('/home/shrini/catkin_ws/src/TurtleSpot/src/dog_bark.wav')
		rospy.sleep(5)
		rospy.loginfo("In SearchWpOne - targete not found")
		return 'not_found'

	def target_pose_cb(self, msg):
		for i in range(len(msg.markers)):
			marker = msg.markers[i]
			if (marker.id == self.target_id):
				self.facing_target = True



class SearchWpTwo(smach.State):
	def __init__(self):
		smach.State.__init__(self, outcomes = ['found', 'not_found'],
								 input_keys = ['target_marker_id'])
		self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)
		rospy.Subscriber("ar_pose_marker", AlvarMarkers, self.target_pose_cb)
		self.angular_vel = 1.0
		# if we are facing target, we will stop spinning
		self.facing_target = False
		self.target_id = ''

	def execute(self, userdata):
		rospy.loginfo('In SearchWpTwo - executing state machine')
		#rospy.loginfo("In Spin - target_detected is" + str(userdata.target_detected))
		rospy.Subscriber("ar_pose_marker", AlvarMarkers, self.target_pose_cb)
		rospy.sleep(3)
		rospy.loginfo("In SearchWpTwo - target_marker_id is" + str(userdata.target_marker_id))
		self.target_id = userdata.target_marker_id

		for i in range(20):
			# if we found the target, stop and exit spinning
			rospy.loginfo("in for loop")
			if self.facing_target:
				sc = SoundClient()
				rospy.sleep(5)
				sc.playWave('/home/turtlebot/ros/hydro/catkin_ws/src/rbx2/rbx2_tasks/nodes/dog_bark.wav')
				#sc.playWave('/home/shrini/catkin_ws/src/TurtleSpot/src/dog_bark.wav')
				rospy.sleep(5)
				rospy.loginfo("Facing target. stopping spin and returning")
				return 'found'
			# else, keep searching by spinning
			move_cmd = Twist()
			move_cmd.linear.x = 0.0
			move_cmd.angular.z = self.angular_vel

			self.cmd_vel_pub.publish(move_cmd)
			rospy.sleep(0.75)
			
		# we finished spinning though we didn't find anything
		sc = SoundClient()
		sc.playWave('/home/turtlebot/ros/hydro/catkin_ws/src/rbx2/rbx2_tasks/nodes/dog_bark.wav')
		#sc.playWave('/home/shrini/catkin_ws/src/TurtleSpot/src/dog_bark.wav')
		rospy.sleep(5)
		rospy.loginfo("In SearchWpTwo - targete not found")
		return 'not_found'

	def target_pose_cb(self, msg):
		for i in range(len(msg.markers)):
			marker = msg.markers[i]
			if (marker.id == self.target_id):
				self.facing_target = True



class SearchWpThree(smach.State):
	def __init__(self):
		smach.State.__init__(self, outcomes = ['found', 'not_found'],
								 input_keys = ['target_marker_id'])
		self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)
		rospy.Subscriber("ar_pose_marker", AlvarMarkers, self.target_pose_cb)
		self.angular_vel = 1.0
		# if we are facing target, we will stop spinning
		self.facing_target = False
		self.target_id = ''

	def execute(self, userdata):
		rospy.loginfo('In SearchWpThree - executing state machine')
		#rospy.loginfo("In Spin - target_detected is" + str(userdata.target_detected))
		rospy.Subscriber("ar_pose_marker", AlvarMarkers, self.target_pose_cb)
		rospy.sleep(3)
		rospy.loginfo("In SearchWpThree - target_marker_id is" + str(userdata.target_marker_id))
		self.target_id = userdata.target_marker_id

		for i in range(20):
			# if we found the target, stop and exit spinning
			rospy.loginfo("in for loop")
			if self.facing_target:
				sc = SoundClient()
				rospy.sleep(5)
				sc.playWave('/home/turtlebot/ros/hydro/catkin_ws/src/rbx2/rbx2_tasks/nodes/dog_bark.wav')
				#sc.playWave('/home/shrini/catkin_ws/src/TurtleSpot/src/dog_bark.wav')
				rospy.sleep(5)
				rospy.loginfo("Facing target. stopping spin and returning")
				return 'found'
			# else, keep searching by spinning
			move_cmd = Twist()
			move_cmd.linear.x = 0.0
			move_cmd.angular.z = self.angular_vel

			self.cmd_vel_pub.publish(move_cmd)
			rospy.sleep(0.75)
			
		# we finished spinning though we didn't find anything
		sc = SoundClient()
		sc.playWave('/home/turtlebot/ros/hydro/catkin_ws/src/rbx2/rbx2_tasks/nodes/dog_bark.wav')
		#sc.playWave('/home/shrini/catkin_ws/src/TurtleSpot/src/dog_bark.wav')
		rospy.sleep(5)
		rospy.loginfo("In SearchWpThree - targete not found")
		return 'not_found'

	def target_pose_cb(self, msg):
		for i in range(len(msg.markers)):
			marker = msg.markers[i]
			if (marker.id == self.target_id):
				self.facing_target = True



class SearchWpFour(smach.State):
	def __init__(self):
		smach.State.__init__(self, outcomes = ['found', 'not_found'],
								 input_keys = ['target_marker_id'])
		self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)
		rospy.Subscriber("ar_pose_marker", AlvarMarkers, self.target_pose_cb)
		self.angular_vel = 1.0
		# if we are facing target, we will stop spinning
		self.facing_target = False
		self.target_id = ''

	def execute(self, userdata):
		rospy.loginfo('In SearchWpFour - executing state machine')
		#rospy.loginfo("In Spin - target_detected is" + str(userdata.target_detected))
		rospy.Subscriber("ar_pose_marker", AlvarMarkers, self.target_pose_cb)
		rospy.sleep(3)
		rospy.loginfo("In SearchWpFour - target_marker_id is" + str(userdata.target_marker_id))
		self.target_id = userdata.target_marker_id

		for i in range(20):
			# if we found the target, stop and exit spinning
			rospy.loginfo("in for loop")
			if self.facing_target:
				sc = SoundClient()
				rospy.sleep(5)
				sc.playWave('/home/turtlebot/ros/hydro/catkin_ws/src/rbx2/rbx2_tasks/nodes/dog_bark.wav')
				#sc.playWave('/home/shrini/catkin_ws/src/TurtleSpot/src/dog_bark.wav')
				rospy.sleep(5)
				rospy.loginfo("Facing target. stopping spin and returning")
				return 'found'
			# else, keep searching by spinning
			move_cmd = Twist()
			move_cmd.linear.x = 0.0
			move_cmd.angular.z = self.angular_vel

			self.cmd_vel_pub.publish(move_cmd)
			rospy.sleep(0.75)
			
		# we finished spinning though we didn't find anything
		sc = SoundClient()
		sc.playWave('/home/turtlebot/ros/hydro/catkin_ws/src/rbx2/rbx2_tasks/nodes/dog_bark.wav')
		#sc.playWave('/home/shrini/catkin_ws/src/TurtleSpot/src/dog_bark.wav')
		rospy.sleep(5)
		rospy.loginfo("In SearchWpFour - targete not found")
		return 'not_found'

	def target_pose_cb(self, msg):
		for i in range(len(msg.markers)):
			marker = msg.markers[i]
			if (marker.id == self.target_id):
				self.facing_target = True



class FollowTag(smach.State):
	def __init__(self):
		smach.State.__init__(self, outcomes = ['succeeded', 'failed'],
							input_keys = ['target_marker_id'])
		self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)
		# target tag we are looking for
		self.target_id = ''
		# constant velocities for robot to run
		self.linear_vel = 0.15
		self.angular_vel = 0.5
		# threshold to stop - after experimenting, values
		self.x_distance_thresh = 0.5
		self.y_distance_thresh = 0.06
		# initialize the distance between robot and marker
		# something high
		self.x_distance = 10.0
		self.y_distance = 5.0
		# error margin between expected and actual distance
		self.err_margin = 0.1

	def execute(self, userdata):
		rospy.loginfo('In FollowTag - executing state machine')
		rospy.Subscriber('ar_pose_marker', AlvarMarkers, self.target_pose_cb)
		rospy.sleep(3)
		self.target_id = userdata.target_marker_id

		while (abs(self.x_distance - self.x_distance_thresh) > self.err_margin):
			rospy.loginfo("X diff is: " + str(abs(self.x_distance - self.x_distance_thresh)))
			rospy.loginfo("Y diff is: " + str(abs(self.y_distance - self.y_distance_thresh)))
			# keep going straight
			move_cmd = Twist()
			move_cmd.linear.x = self.linear_vel
			
			if (abs(self.y_distance - self.y_distance_thresh) > self.err_margin):
				temp = self.angular_vel
				move_cmd.angular.z = copysign(temp, (self.y_distance - self.y_distance_thresh))
			else:
				move_cmd.angular.z = 0.0

			self.cmd_vel_pub.publish(move_cmd)
			rospy.sleep(1.0)

		return 'succeeded'

	def target_pose_cb(self, msg):
		for i in range(len(msg.markers)):
			marker = msg.markers[i]
			if (marker.id == self.target_id):
				# get the x and y distance between target and robot position
				self.x_distance = marker.pose.pose.position.x
				self.y_distance = marker.pose.pose.position.y 



class StopRobot(smach.State):
	def __init__(self):
		smach.State.__init__(self, outcomes = ['succeeded'])
		self.counter = 0
		self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)

	def execute(self, userdata):
		rospy.loginfo('In StopRobot - executing state machine')
		State.request_preempt(self)
		self.cmd_vel_pub.publish(Twist())
		rospy.sleep(1)
		return 'succeeded'


if __name__ == '__main__':
	# initializes all the states and the necessory data
	# to run the state machine and starts the state machine
	try:
		Run()
	except rospy.ROSInterruptException:
		rospy.loginfo("In Run - ROSInterruptException: STATE MACHINE stopped running!!")
