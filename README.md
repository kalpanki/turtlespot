TurtleSpot
===========

This is the package created for the TurtleSpot project done as part of the "Robotics Project" during the 3rd Semester of MSCV. It is based on ROS and depends on some of its packages.

To Execute Search and Rescue
----------------------------
- `real_turtlebot_local.launch` should be launched from the local machine using `roslaunch`.
- `real_turtlebot.launch` should be launched from the laptop onboard the TurtleSpot itself using `roslaunch`.
- On the local machine, run the `run.py` file using `rosrun` and see TurtleSpot in action. 
- Follow the different steps in the workflow.